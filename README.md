# Office Im- & Export

This code is an enhanced version of the code which was part of the [VB and VBA Code Library](https://gitlab.com/juengling/vb-and-vba-code-library), files `ExportExcelCode.bas` and `ExportVisioCode.bas`, but now combined to one simple subroutine.


# Export

If you are working on an VBA project, no matter if Access, Excel, Word
or any of the other MS Office applications, you may want to do some
source code control with your code. Unfortunately Office files are just
containers for many things:

- tables, queries, forms, reports, macros and VBA code in MS Access
- text, UserForms and VBA code in MS Word
- tables, UserForms and VBA code in MS Excel

... and so on, just to name a few.

But don't panic! Exporting the code is quite easy. All you have to do is
allowing access to the VBA object model in the trust center of your favourite
application, import the file `ExportCode.bas` from this repository, and delete
any of the `set activedoc =` lines you don't need for your current application. 

Then execute `DoExport` and you'll find your code in a subfolder named like
your file with a `.src` added. Commit this folder to your favourite source
code control, and you're fine.

If you want to get rid of unmotivated case changes, a close look at my other project
[Casey](https://gitlab.com/juengling/casey) may be helpful.


# Import

And for the **import** part come back in a while ...
